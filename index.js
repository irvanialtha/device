`use strict`;

const 
    { service, amqp, mqtt, subscriber } = require(`./app_module`);

let start = () => {
    return new Promise(async (resolve, reject) => {
        resolve({
            service : await service(),
            amqp    : await amqp(),
            mqtt    : await mqtt(['application/+/device/+/event','outTopic'])
        })
     }).then(async (data) => {
        subscriber();
     });
};

start()