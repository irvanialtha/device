`use strict`;

const
    { errorHandler } = require(`${basedir}/app_module/src/helper`);

module.exports = () => {
    if (global.amqp) {
        global.amqp.on('message',(data)=>{
            const
                target     = data.topic.split('.'),
                requestid  = data.requestid ? data.requestid : null;
            let 
                request, 
                response,
                next;
            
            if(environment.app.type == 'service'){
                try {
                    fileSubscriber    = target[0],
                    functionSubcriber = target[1];
                    
                    subscriber[fileSubscriber][functionSubcriber](data);   
                } catch (error) {
                    errorHandler.service(data, error);
                }
            }

            if(requestid && http_request.hasOwnProperty(requestid)) {
                request  = http_request[requestid].request;
                response = http_request[requestid].response;
                next     = http_request[requestid].next;
                if (data.body.error) {
                    errorHandler.server(response, 200, data);
                    return;
                }
                try {
                    fileSubscriber    = target[0],
                    functionSubcriber = target[1];
                    subscriber[fileSubscriber][functionSubcriber](request, response, data);   
                } catch (error) {
                    errorHandler.server(response, 500, error)
                }
            }
        })
    }
   

    if (global.mqtt) {
        global.mqtt.on('message',(topic, data)=>{
            const
                type       = data.topic ? true : false,
                target     = type ? data.topic.split('.') : topic.split('/'),
                requestid  = data.requestid ? data.requestid : null;
            let 
                request, 
                response,
                next;

            if(environment.app.type == 'service'){
                try {
                    let 
                        fileSubscriber    = type ? target[0] : target[1],
                        functionSubcriber = type ? target[1] : target[2];
                        subscriber.main.data(data)
                        //subscriber[fileSubscriber][functionSubcriber](data);   
                } catch (error) {
                    errorHandler.service(data, error);
                }
            }
            if(requestid && http_request.hasOwnProperty(requestid)) {
                request  = http_request[requestid].request;
                response = http_request[requestid].response;
                next     = http_request[requestid].next;
                if (data.body.error) {
                    errorHandler.server(response, 200, data);
                    return;
                }
                try {
                    fileSubscriber    = target[0],
                    functionSubcriber = target[1];
                    subscriber[fileSubscriber][functionSubcriber](request, response, data);   
                } catch (error) {
                    errorHandler.server(response, 500, error)
                }
            }
        })
    }
}