
module.exports = { 	
    type: `Feature`, 
    deviceType: `Humidity-Temperature`,
    properties: { 
        deviceid		: null, 
        devicename      : null,                     /* device identification number or IMEI or Mac */  
        time			: null,                     /* timestamp */  
        date            : null,
        Current_R		: null,                     /*Humidity in %*/
        Current_S		: null,                      /*Temperature in Celcius*/
        ipAddr          : null,
        ssid            : null,
        rawData			: null                       /* raw data */
	}
};
