
module.exports = { 	
    type: `Feature`, 
    deviceType: `CLTH002`,
    properties: { 
        deviceid		: null, 
        devicename      : null,                     /* device identification number or IMEI or Mac */  
        time			: null,                     /* timestamp */  
        modbus          : null,
        sensor		    : null,                     /*Humidity in %*/
        temp		    : null,                      /*Temperature in Celcius*/
        volt            : null,
        freq            : null,
        activeEnergy	: null                       /* raw data */
	}
};
