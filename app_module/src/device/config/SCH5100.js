
module.exports = { 	
	type: 'Feature', 
	meterType: null,
	properties: { 
		deviceid		: null, /* device identification number or IMEI */  
		devicename		: null, /* Device Name */
		time			: null, /* timestamp */  
		Current_R		: null, /*Current phase R in (A)*/
		Current_S		: null, /*Current phase S in (A)*/
		Current_T		: null, /*Current phase T in (A)*/
		Current_N		: null, /*Current phase N in (A)*/
		Current_G		: null, /*Current phase G in (A)*/
		CurrentAvg		: null, /*Current Average in (A)*/
		CurrentUn_R		: null, /*Current Unbalance phase R in (%)*/
		CurrentUn_S		: null, /*Current Unbalance phase S in (%)*/
		CurrentUn_T		: null, /*Current Unbalance phase T in (%)*/
		CurrentUn_Worst	: null, /*Current Unbalance worst in (%)*/
		Volt_R_S		: null, /*Voltage between phase R and phase S in (V)*/
		Volt_S_T		: null, /*Voltage between phase S and phase T in (V)*/
		Volt_T_R		: null, /*Voltage between phase T and phase R in (V)*/
		Volt_LL_Avg		: null, /*Voltage Line to Line Average in (V)*/
		Volt_R 			: null, /*Voltage between phase R and Neutral in (V)*/
		Volt_S 			: null, /*Voltage between phase S and Neutral in (V)*/
		Volt_T 			: null, /*Voltage between phase T and Neutral in (V)*/
		Volt_LN_Avg		: null, /*Voltage Line to Neutral in (V)*/
		activePwr_R		: null, /*Active Power phase R in (W)*/
		activePwr_S		: null, /*Active Power phase S in (W)*/
		activePwr_T		: null, /*Active Power phase T in (W)*/
		activePwrTotal	: null, /*Active Power Total in (W)*/
		ReactPwr_R		: null, /*Reactive Power phase R in (VAR)*/
		ReactPwr_S		: null, /*Reactive Power phase S in (VAR)*/
		ReactPwr_T		: null, /*Reactive Power phase T in (VAR)*/
		ReactPwrTotal	: null, /*Reactive Power Total in (VAR)*/
		AppPwr_R		: null, /*Apparent Power phase R in (VA)*/
		AppPwr_S		: null, /*Apparent Power phase S in (VA)*/
		AppPwr_T		: null, /*Apparent Power phase T in (VA)*/
		AppPwrTotal		: null, /*Apparent Power Total in (VA)*/
		activeEnergy    : null,  /*Active Energy */
		reactiveEnergy  : null,  /*Reactive Energy */
		PwrFactor_R		: null, /*Power Factor phase R*/
		PwrFactor_S		: null, /*Power Factor phase S*/
		PwrFactor_T		: null, /*Power Factor phase T*/
		PwrFactorTotal	: null, /*Power Factor Total*/  
		Frequency		: null, /*Frequency in (Hz)*/
		battery			: null, /* Battery percentage level 0%-100% */
		batteryLevel	: null, /* [0, 1, 2, 3, 4, 5, 6] */
		batteryLevelDesc: null, /* [0]No Power, [1]Extremely Low, [2]Very Low, [3]Low, [4]Medium, [5]High, [6]Very High */
		signal 			: null, /* [GSM, LoRa, ...] */
		signalLevel		: null, /* [0, 1, 2, 3, 4] */
		signalLevelDesc	: null, /* [0]No Signal, [1]Extremely Weak, [2]Very Weak, [3]Good, [4]Strong */
		rssi			: null, /* Received Signal Strength Indicator level */ 
		lsnr			: null, /* Luminance Signal-to-Noise Ratio level */
		dataSNR			: null, /* Signal-to-noise ratio data */
		dataRSSI		: null, /* Received Signal Strength Indicator data */ 
		dataGWMac		: null, /* Gateway Mac address number */
		dataGWName		: null, /* Gateway Name */
		sourceGW		: null, /* Gateway */
		rawData			: null  /* raw data */
	}
};
