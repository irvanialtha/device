/**
 * @file        : parser.js
 * @author      : Grendy Eki A
 * @description : Powermeter parser core function
 */

'use strict';


// [LOCAL MODULES]
const
    device          = 'SCH5100',
    getdetails      = require(`../status/${device}.js`),
    initPowerJson	= require(`../config/${device}.js`);

// [MODULES]
const   
    hex64 		= require('hex64'),
    binParser   = require('binary-parser').Parser;

// [Parser] 
const
    powerPackage = new binParser()
    .endianess("big")
    .bit8("modStatus")
    .uint16("Current_R", {
        formatter: function(val) { return val * 0.01; }
    }) 
    .uint16("Current_S", {
        formatter: function(val) { return val * 0.01; }
    })
    .uint16("Current_T", {
        formatter: function(val) { return val * 0.01; }
    })
    .uint16("Volt_R_S")
    .uint16("Volt_S_T")
    .uint16("Volt_T_R")
    .uint16("Volt_R")
    .uint16("Volt_S")
    .uint16("Volt_T")
    .uint16("activePwrTotal")
    .uint16("ReactPwrTotal")
    .uint16("AppPwrTotal")
    .uint16("PwrFactorTotal", {
        formatter: function(val) { return val * 0.01; }
    })
    .uint16("Frequency", {
        formatter: function(val) { return val * 0.01; }
    })
    .uint32("activeEnergy")
    .uint32("reactiveEnergy");

// [Generate Powermeter Data]
const main = {
    generate : (msgObj) => {
        return new Promise(async (resolve, reject) => {
            try{
                let powerJson,
                    rawdata		= hex64.toHex(msgObj.data), 
                    rawLength = rawdata.length,
                    payloadjson = powerPackage.parse(Buffer.from(rawdata, 'hex'));
                if (rawLength == 74) {
                    const  
                        deviceid    = msgObj.devEUI,
                        devicename  = msgObj.deviceName,
                        fcount 	    = msgObj.fCnt, 
                        //dvcTime   = new Date(), 
                        //deviceTime= dvcTime.getTime(), 
                        loraFreqArr = new Array((msgObj.txInfo).length),
                        datarateArr = new Array((msgObj.txInfo).length),
                        rssiArray   = new Array((msgObj.rxInfo).length),
                        lsnrArray   = new Array((msgObj.rxInfo).length),
                        gatewayMac  = new Array((msgObj.rxInfo).length),
                        gatewayName = new Array((msgObj.rxInfo).length),
                        gatewayLoc  = new Array((msgObj.rxInfo).length), 
                        gatewayAlt  = new Array((msgObj.rxInfo).length);
                    
                
                    /* signal */
                    let signalLevel;
                    
                    for (let i = 0; i < (msgObj.rxInfo).length; i++) {
                        gatewayMac[i]   = msgObj.rxInfo[i].gatewayID;
                        gatewayName[i]  = msgObj.rxInfo[i].name;
                        loraFreqArr[i]  = parseInt(msgObj.txInfo.frequency);
                        datarateArr[i]  = parseInt(msgObj.txInfo.dr);
                        rssiArray[i]    = parseInt(msgObj.rxInfo[i].rssi, 10);
                        lsnrArray[i]    = parseFloat(msgObj.rxInfo[i].loRaSNR, 10);
                        if(msgObj.rxInfo[i].location){
                            gatewayAlt[i] = parseFloat(msgObj.rxInfo[i].location.altitude, 10);
                            gatewayLoc[i] = [ parseFloat(msgObj.rxInfo[i].location.longitude), parseFloat(msgObj.rxInfo[i].location.latitude)];
                        } else {
                            gatewayAlt[i] = null;
                            gatewayLoc[i] = null;
                        }
                    }  
                    const RssiLevel = Math.max(...rssiArray),
                        rsa = rssiArray.indexOf(Math.max(...rssiArray)),
                        deviceTime   = new Date(msgObj.rxInfo[rsa].time).getTime(),
                        deviceDate   = msgObj.rxInfo[rsa].time,
                        sourceGW     = (msgObj.rxInfo)[rsa].name,
                        lsnr         = lsnrArray[rsa],
                        loraFreq     = loraFreqArr[rsa],
                        datarate     = datarateArr[rsa],
                        GWMacAddress = gatewayMac[rsa],
                        GWLocation   = gatewayLoc[rsa],
                        GWAltitude   = gatewayAlt[rsa];
                        
                    
                    if (RssiLevel >= -105) { //strong
                        signalLevel = 3;
                    } else if (RssiLevel <= -105 && RssiLevel >= -120) { //medium
                        signalLevel = 2;
                    } else if (RssiLevel < -120) { //low
                        signalLevel = 1;
                    } 
                    
                    /* powerJson */
                    powerJson = Object.assign({}, initPowerJson, {
                        type: `Feature`, 
                        meterType: `Schneider`,
                        properties: { 
                            deviceid		: deviceid, 
                            devicename      : devicename,                    /* device identification number or IMEI */  
                            time			: deviceTime,                   /* timestamp */  
                            date            : deviceDate,
                            Current_R		: payloadjson['Current_R'],     /*Current phase R in (A)*/
                            Current_S		: payloadjson['Current_S'],     /*Current phase S in (A)*/
                            Current_T		: payloadjson['Current_T'],     /*Current phase T in (A)*/
                            Volt_R_S		: payloadjson['Volt_R_S'],      /*Voltage between phase R and phase S in (V)*/
                            Volt_S_T		: payloadjson['Volt_S_T'],      /*Voltage between phase S and phase T in (V)*/
                            Volt_T_R		: payloadjson['Volt_T_R'],      /*Voltage between phase T and phase R in (V)*/
                            Volt_R 			: payloadjson['Volt_R'],        /*Voltage between phase R and Neutral in (V)*/
                            Volt_S 			: payloadjson['Volt_S'],        /*Voltage between phase S and Neutral in (V)*/
                            Volt_T 			: payloadjson['Volt_T'],        /*Voltage between phase T and Neutral in (V)*/
                            activePwrTotal	: payloadjson['activePwrTotal'],/*Active Power Total in (W)*/
                            ReactPwrTotal	: payloadjson['ReactPwrTotal'], /*Reactive Power Total in (VAR)*/
                            AppPwrTotal		: payloadjson['AppPwrTotal'],   /*Apparent Power Total in (VA)*/
                            activeEnergy    : payloadjson['activeEnergy'],  /*Active Energy */
                            reactiveEnergy  : payloadjson['reactiveEnergy'],  /*Reactive Energy */
                            PwrFactorTotal	: payloadjson['PwrFactorTotal'],/*Power Factor Total*/
                            Frequency		: payloadjson['Frequency'],     /*Frequency in (Hz)*/
                            signal 			: 'LoRa',                       /* [GSM, LoRa, ...] */
                            signalLevel		: signalLevel,                  /* [0, 1, 2, 3, 4] */
                            signalLevelDesc	: getdetails.signalLevelDesc(signalLevel), /* [0]No Signal, [1]Extremely Weak, [2]Very Weak, [3]Good, [4]Strong */
                            loraFreq		: loraFreq,
                            datarate		: datarate,
                            fcount			: fcount,
                            rssi			: RssiLevel,                    /* Received Signal Strength Indicator level */ 
                            lsnr			: lsnr,                         /* Luminance Signal-to-Noise Ratio level */
                            dataSNR			: lsnrArray,                    /* Signal-to-noise ratio data */
                            dataRSSI		: rssiArray,                    /* Received Signal Strength Indicator data */ 
                            dataGWMac		: gatewayMac,                   /* Gateway Mac address number */
                            dataGWName		: gatewayName,                  /* Gateway Name */
                            dataGWLoc		: gatewayLoc,
                            dataGWAlt		: gatewayAlt,
                            sourceGWMac		: GWMacAddress,
                            sourceGW		: sourceGW,                     /* Gateway */
                            sourceGWLoc		: GWLocation,
                            sourceGWAlt		: GWAltitude,
                            rawData			: rawdata                       /* raw data */
                        }
                });

                resolve(powerJson)
                }
            }
            catch(error) {
                console.log(error)
                resolve({error:JSON.stringify(error)})
            }
        })
        
    }

};
module.exports = { ... main };
