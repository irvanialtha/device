/**
 * @file        : parser.js
 * @author      : Grendy Eki A
 * @description : Calathu smart temperatur v1 parser core function
 */

'use strict';


// [LOCAL MODULES]
const
    device          = 'CLTH0001',
//  getdetails      = require(`../status/${device}.js`),
    initClth0001Json	= require(`../config/${device}.js`);

// [MODULES]
const   
    binParser   = require('binary-parser').Parser;

// [Parser] 
const
    clth0001Package = new binParser()
    .endianess("big")
    .uint16("Humidity", {
        formatter: function(val) { return val * 0.01; }
    }) 
    .uint16("Temperature", {
        formatter: function(val) { return val * 0.01; }
    });

// [Generate Calathu 0001 Data]
const main = {
    generate : (msgObj) => {
        return new Promise(async (resolve, reject) => {
            try{
                let clth0001Json,
                    rawdata		= msgObj.data,
                    rawLength = rawdata.length,
                    payloadjson = clth0001Package.parse(Buffer.from(rawdata, 'hex'));
    
                if (rawLength == 8 && payloadjson['modStatus'] == 255) { 
                    const 
                        deviceid    = msgObj.macDevice,
                        devicename  = msgObj.deviceName,
                        ipAddr      = msgObj.ipAddr,
                        ssid        = msgObj.ssid;

                    /* clth0001Json */
                    clth0001Json = Object.assign({}, initClth0001Json, {
                        type: `Feature`, 
                        deviceType: `Humidity-Temperature`,
                        properties: { 
                            deviceid		: deviceid, 
                            devicename      : devicename,                       /* device identification number or IMEI or Mac */  
                            time			: deviceTime,                       /* timestamp */  
                            date            : deviceDate,
                            Current_R		: payloadjson['Humidity'],          /*Humidity in %*/
                            Current_S		: payloadjson['Temperature'],       /*Temperature in Celcius*/
                            ipAddr          : ipAddr,
                            ssid            : ssid,
                            rawData			: rawdata                           /* raw data */
                        }
                    });
    
                    resolve(clth0001Json)
                }
            }
            catch(error) {
                console.log(error)
                resolve({error:JSON.stringify(error)})
            }
        })
        
    }

};
module.exports = { ... main };
