/**
 * @file        : parser.js
 * @author      : Grendy Eki A
 * @description : Calathu smart temperatur v1 parser core function
 */

'use strict';


// [LOCAL MODULES]
const
    device  = 'CLTH002',
    config	= require(`../config/${device}.js`),
    base64  = require('base-64');

// [Generate Calathu CLTH002 Data]
const main = {
    generate : (msgObj) => {
        return new Promise(async (resolve, reject) => {
            try{
                let parsingData,
                    device  = msgObj.deviceName.split('_'),
                    time    = msgObj.time * 1000,
                    _data   = JSON.parse(base64.decode(msgObj.payload));

                /* CLTH002 */
                parsingData = Object.assign({}, config, {
                    type: `Feature`, 
                    deviceType: `CLTH002`,
                    properties: { 
                        deviceid		: device[1], 
                        devicename      : msgObj.deviceName,                     /* device identification number or IMEI or Mac */  
                        time			: time,                       
                        modbus          : _data.modbus,
                        sensor		    : _data.sensor,                     
                        temp		    : _data.temp,                      
                        volt            : _data.volt,
                        freq            : _data.freq,
                        activeEnergy	: _data.activeEnergy                      
                    }
                });
                resolve(parsingData)
            }
            catch(error) {
                console.log(error)
                resolve({error:JSON.stringify(error)})
            }
        })
        
    }

};
module.exports = { ... main };
