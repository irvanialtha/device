`use strict`;
const
    device      = require(`${basedir}/app_module/src/device`),
    { time }    = require(`${basedir}/app_module/src/helper`);
module.exports = {
    data: async (data) =>{
        try {
            let deviceName = data.deviceName.split('_')[2]
                dataParse  = await device[deviceName].generate(data);
            
            if (dataParse && dataParse.error) {
                console.log(`${time.today(true)} : ${dataParse}`);
                return;
            }
            if (typeof dataParse !== 'undefined') {
                model.main.parse(dataParse);
            }
            
        } catch (error) {
            console.log(`${time.today(true)} : ${error}`);
        }
    }
}