`use strict`;

module.exports = (route) => {

    route.get(`/`, (req, res) => {
        model.main.menu(req,res);
    });

    route.post(`/sample`, (req, res) => {
        control.main.sample(req, res);
    });

    return route;
}