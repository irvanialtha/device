/**
 * @file        : submenu.js
 * @author      : Adventus Wijaya Lumbantobing <adventus.wijaya@elnusa.co.id>
 * @description : Database menu's table mapping
 */

`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`menu`, {
        menu_uid          : {
            type       : Sequelize.UUIDV4,
            primaryKey : true,
            defaultValue: Sequelize.UUIDv4
        },
        name        : {
            type      : Sequelize.STRING,
            allowNull : false,
        },
        description : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        url         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        menu_group_uid     : {
            type        : Sequelize.UUIDV4,
            allowNull   : false,
            references : {
                model : `menu_group`,
                key   : `menu_group_uid`
            }
        },
        sequence  : {
            type      : Sequelize.INTEGER,
            allowNull : true
        },
        icon      : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        createdate    : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        createby      : {
            type      : Sequelize.STRING,
            allowNull : true
        }
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'public'
    });

};